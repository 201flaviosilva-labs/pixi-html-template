# Pixi Template

## Description

Basic pixi HTML project Template

## Preview

![Preview](./Preview.png)

## Links and References

- [Pixijs](https://pixijs.com/);
- [Code](https://bitbucket.org/201flaviosilva-labs/pixi-html-template/);
