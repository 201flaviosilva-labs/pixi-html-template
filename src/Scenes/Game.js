import assets from "../assets.js";
import config from "../config.js";

export default class Game extends PIXI.Container {
	constructor() {
		super();
		this._width = 50;
		this._height = 100;

		this.graphics = new PIXI.Graphics();
		this.addChild(this.graphics);

		this.addText();
		this.addSprites();

		this.draw();
	}

	set width(value) {
		this._width = value;
		this.draw();
	}

	set height(value) {
		this._height = value;
		this.draw();
	}

	get width() { this._width; }
	get height() { this._height; }

	addText() {
		const style = new PIXI.TextStyle({
			fill: "white",
			fontFamily: "\"Lucida Console\", Monaco, monospace",
			fontSize: 30,
			lineHeight: 36,
			leading: 19,

			// Shadow
			dropShadow: true,
			dropShadowAngle: 1,
			dropShadowBlur: 10,
			dropShadowColor: "#b80000",
			dropShadowDistance: 2,
		});
		const text = new PIXI.Text("Hello World", style);
		this.addChild(text);
	}

	addSprites() {
		const texture = PIXI.Texture.from(assets[0]);
		const sprite = new PIXI.Sprite(texture);
		const x = config.width / 2 - sprite.width / 2;
		const y = config.height / 2 - sprite.height / 2;
		sprite.position.set(x, y);
		this.addChild(sprite);
	}

	draw() {
		this.graphics.clear();

		this.graphics.beginFill(0xffffff, 0.7);
		this.graphics.drawRect(0, 0, this._width, this._height);
		this.graphics.endFill();
	}
}
