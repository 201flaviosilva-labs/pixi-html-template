import config from "./config.js";
import assets from "./assets.js";

import Game from "./Scenes/Game.js";

const app = new PIXI.Application(config);
app.renderer.resize(config.width, config.height);
app.renderer.autoResize = true;
document.body.appendChild(app.view);

// PIXI.loader
// 	.add(assets)
// 	.load(start());

start();
function start() {
	const game = new Game();
	game.position.y = 10;
	game.position.x = 10;
	app.stage.addChild(game);
}
